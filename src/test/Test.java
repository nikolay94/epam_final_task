package test;

import bean.Card;
import bean.CardType;
import bean.Game;
import bean.User;
import command.impl.account.SignUp;
import database.dao.DAOFactory;
import database.dao.impl.UserDAO;
import org.junit.Assert;
import org.junit.Before;

import java.util.LinkedList;

/**
 * JUnit class.
 */
public class Test {

    private Game game;
    private User user;
    private UserDAO userDAO;
    /**
     * Initialization data before the test.
     */
    @Before
    public void setUp(){
        user = new User();
        user.setId(2);
        user.setNickname("Quinky");
        user.setEmail("filippov.nik94@gmail.com");
        game = new Game();
        userDAO = DAOFactory.getInstance().getUserDAO();
        LinkedList<Card> cards = new LinkedList();
        cards.add(new Card(CardType.TWO,""));
        cards.add(new Card(CardType.KING,""));
        cards.add(new Card(CardType.ACE,""));
        game.setMyCards(cards);
    }
    /**
     * Test amount of points of list of cards.
     * @see Game
     */
    @org.junit.Test
    public void testPoints(){
        Assert.assertEquals(13,game.getPoints(game.getMyCards()));
    }
    /**
     * Test if email consists in the database.
     * @see UserDAO
     */
    @org.junit.Test
    public void isUserExistsWithEmail(){
        Assert.assertEquals(true, userDAO.isUserExistsWithEmail("filippov.nik94@gmail.com"));
    }
    /**
     * Test if nickname consists in the database.
     * @see UserDAO
     */
    @org.junit.Test
    public void isUserExistsWithNickname(){
        Assert.assertEquals(true,userDAO.isUserExistsWithNickname("Quinky"));
    }
    /**
     * Test get user by it's id.
     * @see UserDAO
     */
    @org.junit.Test
    public void testGetUserById(){
        Assert.assertEquals(user,userDAO.getEntityById(user.getId()));
    }
    /**
     * Test get user by it's nickname.
     * @see UserDAO
     */
    @org.junit.Test
    public void testGetUserByNickname(){
        Assert.assertEquals(user,userDAO.getEntityByNickname(user.getNickname()));
    }

}
